package com.example.bloodbank.presentation.Slider.Adapter;

import com.example.bloodbank.presentation.Slider.View.SliderFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SlideAdapter extends FragmentPagerAdapter {
    public SlideAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return SliderFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}