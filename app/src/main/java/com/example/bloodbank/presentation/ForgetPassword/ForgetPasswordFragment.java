package com.example.bloodbank.presentation.ForgetPassword;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bloodbank.Base.BaseFragment;
import com.example.bloodbank.R;

public class ForgetPasswordFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_password_forget, container, false);
        setUpActivity();

        return view;
    }

}
