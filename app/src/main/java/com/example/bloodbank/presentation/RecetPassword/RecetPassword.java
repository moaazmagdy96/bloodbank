package com.example.bloodbank.presentation.RecetPassword;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bloodbank.Base.BaseFragment;
import com.example.bloodbank.R;

public class RecetPassword extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_password_recent, container, false);
        setUpActivity();

        return view;
    }

}
