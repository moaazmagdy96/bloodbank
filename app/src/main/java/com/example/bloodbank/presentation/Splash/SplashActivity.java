package com.example.bloodbank.presentation.Splash;

import android.content.Intent;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.bloodbank.R;
import com.example.bloodbank.presentation.Slider.View.sliderCycleActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                Intent go = new Intent(SplashActivity.this, sliderCycleActivity.class);
                startActivity(go);
            }
        };

        handler.postDelayed(run,3000);


    }




}
