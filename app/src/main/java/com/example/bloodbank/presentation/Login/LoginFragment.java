package com.example.bloodbank.presentation.Login;

import butterknife.BindView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.bloodbank.Base.BaseFragment;
import com.example.bloodbank.R;
import com.example.bloodbank.Base.api.ApiService;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.ed_phone_login)
    EditText ed_phone_login;
    @BindView(R.id.ed_pass_login)
    EditText ed_pass_login;
    ApiService apiService;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        setUpActivity();
        return view;
    }

}