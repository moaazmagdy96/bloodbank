package com.example.bloodbank.Base.api;



import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {
    @POST("login")
    @FormUrlEncoded
    Call getLogin(@Field("phone") String phone,
                            @Field("password") String password);
}